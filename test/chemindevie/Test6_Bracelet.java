package chemindevie;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static chemindevie.Pierre.*;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;

public class Test6_Bracelet {

    @Test
    public void braceletArnaud() {
        Bracelet bracelet = Bracelet.calculer(Arrays.asList(
                new String[]{"Arnaud", "Michel"}),
                "Lanoix", "Adamyk",
                16, 7, 1979);
        List<Pierre> pierres = bracelet.pierres();
        System.out.println(pierres);
        assertIterableEquals(
                Arrays.asList(new Pierre[]{AIGUE_MARINE, MALACHITE, JADE, OBSIDIENNE, GRENAT, AMETHYSTE, AMBRE, AMETHYSTE}),
                bracelet.pierres());
    }

    @Test
    public void braceletIronman() {
        Bracelet bracelet = Bracelet.calculer(Arrays.asList(
                new String[]{"Anthony", "Edward"}),
                "Stark", "Carbonell",
                29, 5, 1970);
        assertIterableEquals(
                Arrays.asList(new Pierre[]{RHODOCHROSITE, OPALE, GRENAT, SEL, AIGUE_MARINE, GRENAT, OBSIDIENNE, OPALE}),
                bracelet.pierres());
    }

    @Test
    public void braceletBatman() {
        Bracelet bracelet = Bracelet.calculer(Arrays.asList(
                new String[]{"Bruce"}),
                "Wayne", "Kane",
                19, 2, 1972);
        assertIterableEquals(
                Arrays.asList(new Pierre[]{AIGUE_MARINE, MALACHITE, CRISTAL, FLUORITE, SOUFRE, HEMATITE, EMERAUDE, EMERAUDE}),
                bracelet.pierres());
    }

}
