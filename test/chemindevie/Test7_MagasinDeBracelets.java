package chemindevie;

import magasin.exceptions.*;
import magasin.iArticle;
import magasin.iClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static chemindevie.Pierre.*;
import static org.junit.jupiter.api.Assertions.*;

public class Test7_MagasinDeBracelets {

    iMagasinDeBracelets magasin;

    @BeforeEach
    public void init() {
        magasin = new MagasinDeBracelets();
    }

    @Test
    public void stockVide() {
        assertTrue(magasin.listerLesPierresEnStock().isEmpty());
    }

    @Test
    public void stockerPierreLune() throws QuantiteNegativeOuNulleException {
        magasin.ajouterDesPierres(PIERRE_LUNE, 10);
        List<Map.Entry<iArticle, Integer>> result = magasin.listerLesPierresEnStock();
        assertAll(
                () -> assertEquals(1, result.size()),
                () -> assertEquals(PIERRE_LUNE, result.get(0).getKey()),
                () -> assertEquals(10, result.get(0).getValue())
        );
    }

    @Test
    public void stockerPierreLune_exception() throws QuantiteNegativeOuNulleException {
        assertThrows(QuantiteNegativeOuNulleException.class, () -> magasin.ajouterDesPierres(PIERRE_LUNE, -10));
    }

    @Test
    public void stockerPierreLuneObsidienne() throws QuantiteNegativeOuNulleException {
        magasin.ajouterDesPierres(PIERRE_LUNE, 10);
        magasin.ajouterDesPierres(OBSIDIENNE, 15);
        List<Map.Entry<iArticle, Integer>> result = magasin.listerLesPierresEnStock();
        assertAll(
                () -> assertEquals(2, result.size()),
                () -> assertEquals(PIERRE_LUNE, result.get(1).getKey()),
                () -> assertEquals(10, result.get(1).getValue()),
                () -> assertEquals(OBSIDIENNE, result.get(0).getKey()),
                () -> assertEquals(15, result.get(0).getValue())
        );
    }

    @Test
    public void stockerPierreLuneObsidienne_reapro() throws QuantiteNegativeOuNulleException {
        magasin.ajouterDesPierres(PIERRE_LUNE, 10);
        magasin.ajouterDesPierres(OBSIDIENNE, 15);
        magasin.ajouterDesPierres(PIERRE_LUNE, 15);
        List<Map.Entry<iArticle, Integer>> result = magasin.listerLesPierresEnStock();
        assertAll(
                () -> assertEquals(2, result.size()),
                () -> assertEquals(PIERRE_LUNE, result.get(1).getKey()),
                () -> assertEquals(25, result.get(1).getValue()),
                () -> assertEquals(OBSIDIENNE, result.get(0).getKey()),
                () -> assertEquals(15, result.get(0).getValue())
        );
    }

    @Test
    public void stockerPierres() throws QuantiteNegativeOuNulleException {
        remplirDePierres();
        List<Map.Entry<iArticle, Integer>> result = magasin.listerLesPierresEnStock();
        assertAll(
                () -> assertEquals(33, result.size()),
                () -> assertEquals(PIERRE_LUNE, result.get(PIERRE_LUNE.reference() - 1).getKey()),
                () -> assertEquals(10, result.get(PIERRE_LUNE.reference() - 1).getValue()),
                () -> assertEquals(OBSIDIENNE, result.get(OBSIDIENNE.reference() - 1).getKey()),
                () -> assertEquals(10, result.get(OBSIDIENNE.reference() - 1).getValue())
        );
    }


    @Test
    public void ajouterClient() throws ClientDejaEnregistreException {
        Client ironman = new Client("Starck", "Tony", "iron@man.com", "ironman");
        magasin.enregistrerUnClient(ironman);
        List<iClient> clients = magasin.listerLesClients();
        assertAll(
                () -> assertEquals(1, clients.size()),
                () -> assertTrue(clients.contains(ironman))
        );
    }

    @Test
    public void ajouterClients() throws ClientDejaEnregistreException {
        Client ironman = new Client("Stark", "Tony", "tony@stark.com", "ironman");
        Client batman = new Client("Wayne", "Bruce", "bruce@wyane.com", "batman");
        magasin.enregistrerUnClient(ironman);
        magasin.enregistrerUnClient(batman);
        assertIterableEquals(Arrays.asList(new Client[]{batman, ironman}), magasin.listerLesClients());
    }

    @Test
    public void ajouterClient2fois() throws ClientDejaEnregistreException {
        Client ironman = new Client("Stark", "Tony", "tony@stark.com", "ironman");
        Client batman = new Client("Wayne", "Bruce", "bruce@wyane.com", "batman");
        magasin.enregistrerUnClient(ironman);
        magasin.enregistrerUnClient(batman);
        assertThrows(ClientDejaEnregistreException.class, () -> magasin.enregistrerUnClient(ironman));
    }


    @Test
    public void commanderUnBracelet() throws ClientDejaEnregistreException, QuantiteNegativeOuNulleException,
            QuantiteEnStockInsuffisanteException, ClientInconnuException, ArticleHorsStockException {
        remplirDePierres();
        Client ironman = new Client("Stark", "Tony", "tony@stark.com", "ironman");
        magasin.enregistrerUnClient(ironman);
        Bracelet ironBracelet = Bracelet.calculer(Arrays.asList(
                new String[]{"Anthony", "Edward"}),
                "Stark", "Carbonell",
                29, 5, 1970);

        magasin.ajouterUnBracelet(ironBracelet, ironman);

        List<Map.Entry<iArticle, Integer>> result = magasin.listerLesPierresEnStock();
        assertAll(
                () -> assertEquals(9, result.get(RHODOCHROSITE.reference() - 1).getValue()),
                () -> assertEquals(8, result.get(OPALE.reference() - 1).getValue()),
                () -> assertEquals(8, result.get(GRENAT.reference() - 1).getValue()),
                () -> assertEquals(9, result.get(SEL.reference() - 1).getValue()),
                () -> assertEquals(9, result.get(AIGUE_MARINE.reference() - 1).getValue()),
                () -> assertEquals(9, result.get(OBSIDIENNE.reference() - 1).getValue()),
                () -> assertEquals(8 * 15.99, magasin.montantDuPanier(ironman), 0.001)
        );
    }

    @Test
    public void commanderPlusieursBracelets() throws ClientDejaEnregistreException, QuantiteNegativeOuNulleException,
            QuantiteEnStockInsuffisanteException, ClientInconnuException, ArticleHorsStockException {
        remplirDePierres();
        Client ironman = new Client("Stark", "Tony", "tony@stark.com", "ironman");
        magasin.enregistrerUnClient(ironman);
        Bracelet ironBracelet = Bracelet.calculer(Arrays.asList(
                new String[]{"Anthony", "Edward"}),
                "Stark", "Carbonell",
                29, 5, 1970);
        Bracelet batBracelet = Bracelet.calculer(Arrays.asList(
                new String[]{"Bruce"}),
                "Wayne", "Kane",
                19, 2, 1972);


        magasin.ajouterUnBracelet(ironBracelet, ironman);
        magasin.ajouterUnBracelet(batBracelet, ironman);

        List<Map.Entry<iArticle, Integer>> result = magasin.listerLesPierresEnStock();
        assertAll(
                () -> assertEquals(9, result.get(RHODOCHROSITE.reference() - 1).getValue()),
                () -> assertEquals(8, result.get(OPALE.reference() - 1).getValue()),
                () -> assertEquals(8, result.get(GRENAT.reference() - 1).getValue()),
                () -> assertEquals(9, result.get(SEL.reference() - 1).getValue()),
                () -> assertEquals(8, result.get(AIGUE_MARINE.reference() - 1).getValue()),
                () -> assertEquals(9, result.get(OBSIDIENNE.reference() - 1).getValue()),
                () -> assertEquals(9, result.get(MALACHITE.reference() - 1).getValue()),
                () -> assertEquals(9, result.get(CRISTAL.reference() - 1).getValue()),
                () -> assertEquals(9, result.get(FLUORITE.reference() - 1).getValue()),
                () -> assertEquals(9, result.get(SOUFRE.reference() - 1).getValue()),
                () -> assertEquals(9, result.get(HEMATITE.reference() - 1).getValue()),
                () -> assertEquals(8, result.get(EMERAUDE.reference() - 1).getValue()),
                () -> assertEquals(2 * 8 * 15.99, magasin.montantDuPanier(ironman), 0.001)
        );
    }

    @Test
    public void commanderUnBracelet_2fois() throws ClientDejaEnregistreException, QuantiteNegativeOuNulleException,
            QuantiteEnStockInsuffisanteException, ClientInconnuException, ArticleHorsStockException {
        remplirDePierres();
        Client ironman = new Client("Stark", "Tony", "tony@stark.com", "ironman");
        magasin.enregistrerUnClient(ironman);
        Bracelet ironBracelet = Bracelet.calculer(Arrays.asList(
                new String[]{"Anthony", "Edward"}),
                "Stark", "Carbonell",
                29, 5, 1970);

        magasin.ajouterUnBracelet(ironBracelet, ironman);
        magasin.ajouterUnBracelet(ironBracelet, ironman);

        List<Map.Entry<iArticle, Integer>> result = magasin.listerLesPierresEnStock();
        assertAll(
                () -> assertEquals(8, result.get(RHODOCHROSITE.reference() - 1).getValue()),
                () -> assertEquals(6, result.get(OPALE.reference() - 1).getValue()),
                () -> assertEquals(6, result.get(GRENAT.reference() - 1).getValue()),
                () -> assertEquals(8, result.get(SEL.reference() - 1).getValue()),
                () -> assertEquals(8, result.get(AIGUE_MARINE.reference() - 1).getValue()),
                () -> assertEquals(8, result.get(OBSIDIENNE.reference() - 1).getValue()),
                () -> assertEquals(2 * 8 * 15.99, magasin.montantDuPanier(ironman), 0.001)
        );
    }

    @Test
    public void commanderUnBracelet_5fois() throws ClientDejaEnregistreException, QuantiteNegativeOuNulleException,
            QuantiteEnStockInsuffisanteException, ClientInconnuException, ArticleHorsStockException {
        remplirDePierres();
        Client ironman = new Client("Stark", "Tony", "tony@stark.com", "ironman");
        magasin.enregistrerUnClient(ironman);
        Bracelet ironBracelet = Bracelet.calculer(Arrays.asList(
                new String[]{"Anthony", "Edward"}),
                "Stark", "Carbonell",
                29, 5, 1970);

        magasin.ajouterUnBracelet(ironBracelet, ironman);
        magasin.ajouterUnBracelet(ironBracelet, ironman);
        magasin.ajouterUnBracelet(ironBracelet, ironman);
        magasin.ajouterUnBracelet(ironBracelet, ironman);
        magasin.ajouterUnBracelet(ironBracelet, ironman);

        List<Map.Entry<iArticle, Integer>> result = magasin.listerLesPierresEnStock();
        assertAll(
                () -> assertEquals(5, result.get(RHODOCHROSITE.reference() - 1).getValue()),
                () -> assertEquals(0, result.get(OPALE.reference() - 1).getValue()),
                () -> assertEquals(0, result.get(GRENAT.reference() - 1).getValue()),
                () -> assertEquals(5, result.get(SEL.reference() - 1).getValue()),
                () -> assertEquals(5, result.get(AIGUE_MARINE.reference() - 1).getValue()),
                () -> assertEquals(5, result.get(OBSIDIENNE.reference() - 1).getValue()),
                () -> assertEquals(5 * 8 * 15.99, magasin.montantDuPanier(ironman), 0.001)
        );
    }

    @Test
    public void commanderUnBracelet_6fois() throws ClientDejaEnregistreException, QuantiteNegativeOuNulleException,
            QuantiteEnStockInsuffisanteException, ClientInconnuException, ArticleHorsStockException {
        remplirDePierres();
        Client ironman = new Client("Stark", "Tony", "tony@stark.com", "ironman");
        magasin.enregistrerUnClient(ironman);
        Bracelet ironBracelet = Bracelet.calculer(Arrays.asList(
                new String[]{"Anthony", "Edward"}),
                "Stark", "Carbonell",
                29, 5, 1970);

        magasin.ajouterUnBracelet(ironBracelet, ironman);
        magasin.ajouterUnBracelet(ironBracelet, ironman);
        magasin.ajouterUnBracelet(ironBracelet, ironman);
        magasin.ajouterUnBracelet(ironBracelet, ironman);
        magasin.ajouterUnBracelet(ironBracelet, ironman);

        assertThrows(QuantiteEnStockInsuffisanteException.class, () -> magasin.ajouterUnBracelet(ironBracelet, ironman));

    }

    @Test
    public void randomUser_1() {
        magasin.importerDesClientsDepuisRandomUser(1);
        assertEquals(1, magasin.listerLesClients().size());
    }

    @Test
    public void randomUser_10() {
        magasin.importerDesClientsDepuisRandomUser(10);
        assertEquals(10, magasin.listerLesClients().size());
    }

    @Test
    public void compare_Random_10() {
        magasin.importerDesClientsDepuisRandomUser(10);
        MagasinDeBracelets m2 = new MagasinDeBracelets();
        m2.importerDesClientsDepuisRandomUser(10);
        magasin.listerLesClients().retainAll(m2.listerLesClients());
        assertEquals(10, magasin.listerLesClients().size());


    }


    private void remplirDePierres() throws QuantiteNegativeOuNulleException {
        magasin.ajouterDesPierres(QUARTZ_ROSE, 10);
        magasin.ajouterDesPierres(JASPE, 10);
        magasin.ajouterDesPierres(CALCEDOINE, 10);
        magasin.ajouterDesPierres(JADE, 10);
        magasin.ajouterDesPierres(EMERAUDE, 10);
        magasin.ajouterDesPierres(GRENAT, 10);
        magasin.ajouterDesPierres(CITRINE, 10);
        magasin.ajouterDesPierres(OBSIDIENNE, 10);
        magasin.ajouterDesPierres(AIGUE_MARINE, 10);
        magasin.ajouterDesPierres(RHODOCHROSITE, 10);
        magasin.ajouterDesPierres(CORNALINE, 10);
        magasin.ajouterDesPierres(AMBRE, 10);
        magasin.ajouterDesPierres(HEMATITE, 10);
        magasin.ajouterDesPierres(AMETHYSTE, 10);
        magasin.ajouterDesPierres(MALACHITE, 10);
        magasin.ajouterDesPierres(OPALE, 10);
        magasin.ajouterDesPierres(TURQUOISE, 10);
        magasin.ajouterDesPierres(PIERRE_LUNE, 10);
        magasin.ajouterDesPierres(TOPAZE, 10);
        magasin.ajouterDesPierres(LAPIS_LAZULI, 10);
        magasin.ajouterDesPierres(TOURMALINE, 10);
        magasin.ajouterDesPierres(CRISTAL, 10);
        magasin.ajouterDesPierres(AZURITE, 10);
        magasin.ajouterDesPierres(AMAZONITE, 10);
        magasin.ajouterDesPierres(OEIL_TIGRE, 10);
        magasin.ajouterDesPierres(PYRITE, 10);
        magasin.ajouterDesPierres(FLUORITE, 10);
        magasin.ajouterDesPierres(PERLE, 10);
        magasin.ajouterDesPierres(SODALITE, 10);
        magasin.ajouterDesPierres(QUARTZ_FUME, 10);
        magasin.ajouterDesPierres(SOUFRE, 10);
        magasin.ajouterDesPierres(MERCURE, 10);
        magasin.ajouterDesPierres(SEL, 10);
    }
}
