package magasin;

import magasin.exceptions.ArticleHorsPanierException;
import magasin.exceptions.QuantiteNegativeOuNulleException;
import magasin.exceptions.QuantiteSuppPanierException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * défini une commande, c'est-à-dire des articles associés à leur quantité commandée
 */

public class Commande implements Comparable<Commande> {

    private HashMap<iArticle, Integer> articles;

    public Commande() {
        this.articles = new HashMap<>();
    }

    /**
     * indique si la commande est vide
     *
     * @return estVide boolean indiquant si la commande est vide
     */
    public boolean estVide() {
        return this.articles.isEmpty();
    }

    /**
     * ajoute la quantité indiquée de l'article considéré  à la commande
     *
     * @param quantite        quantité à ajouter
     * @param articleCommande article à considérer
     * @throws QuantiteNegativeOuNulleException si la quantité indiquée est négative ou nulle
     */
    public void ajout(int quantite, iArticle articleCommande)
            throws QuantiteNegativeOuNulleException {
        if (quantite <= 0)
            throw new QuantiteNegativeOuNulleException();
        this.articles.putIfAbsent(articleCommande, 0);
        this.articles.replace(articleCommande, this.articles.get(articleCommande) + quantite);
    }

    /**
     * retire de la commande la quantité indiquée de l'article considéré
     *
     * @param quantite        quantité à retirer
     * @param articleCommande article à considérer
     * @throws QuantiteNegativeOuNulleException si la quantité indiquée est négative ou nulle
     * @throws QuantiteSuppPanierException      si la quantité indiquée est supp à celle dans da commande
     * @throws ArticleHorsPanierException       si l'article considéré n'est pas dans la commande
     */
    public void retirer(int quantite, iArticle articleCommande)
            throws QuantiteNegativeOuNulleException,
            QuantiteSuppPanierException, ArticleHorsPanierException {
        if (quantite <= 0)
            throw new QuantiteNegativeOuNulleException();
        if (!this.articles.containsKey(articleCommande))
            throw new ArticleHorsPanierException();
        int quantity = this.articles.get(articleCommande);
        if (quantity < quantite)
            throw new QuantiteSuppPanierException();
        if (quantity - quantite == 0)
            this.articles.remove(articleCommande);
        else
            this.articles.replace(articleCommande, quantity - quantite);
    }

    /**
     * donne une liste de tous les articles présent dans la commande
     * (trié par nom d'article)
     *
     * @return articlesParNom la liste d'articles triée par nom
     */
    public List<iArticle> listerArticlesParNom() {
        List<iArticle> data = new ArrayList<>(this.articles.keySet());
        data.sort(iArticle.COMPARATEUR_NOM);
        return data;
    }

    /**
     * donne une liste de tous les articles présent dans la commande
     * (trié par reference)
     *
     * @return articlesParReference la liste d'articles triée par référence
     */
    public List<iArticle> listerArticlesParReference() {
        List<iArticle> data = new ArrayList<>(this.articles.keySet());
        data.sort(iArticle.COMPARATEUR_REFERENCE);
        return data;
    }

    /**
     * donne une liste de tous les couples (articleCommande, quantiteCommandee)
     * présent dans la commande
     *
     * @return resuméDeLaCommande la liste des articlés et leur quantité trié par nom
     */
    public List<Map.Entry<iArticle, Integer>> listerCommande() {
        List<Map.Entry<iArticle, Integer>> data = new ArrayList<>(this.articles.entrySet());
        data.sort((o1, o2) -> iArticle.COMPARATEUR_NOM.compare(o1.getKey(), o2.getKey()));
        return data;
    }


    /**
     * donne la quantité commandée de l'article considéré
     *
     * @param article l'article à considérer
     * @return la quantité commmandée
     */
    public int quantite(iArticle article) {
        return this.articles.getOrDefault(article, 0);
    }

    /**
     * donne le montant actuel de la commande
     *
     * @return montant le montant total de la commande
     */
    public double montant() {
        double price = 0;
        for (Map.Entry<iArticle, Integer> entry: this.articles.entrySet())
            price += entry.getKey().prix() * entry.getValue();
        return price;
    }

    /**
     * Méthode permettant de comparer 2 commandes via leur montant
     * @param o commande à comparer
     * @return comparaisonInt
     */
    @Override
    public int compareTo(Commande o) {
        return Double.compare(o.montant(), this.montant());
    }
}
