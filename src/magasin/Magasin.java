package magasin;

import magasin.exceptions.*;

import java.util.*;

public class Magasin implements iStock, iClientele, iPanier {

    /**
     * La variable stock nous permet de gérer la quantité de chaque produit enregistré.
     * La variable paniers nous permet de gérer les paniers courant de chaque client enregistré.
     * La variable commandes nous permet de consulter l'historique d'achat de n'importe quel client.
     * Cette dernière nous sert également à générer notre liste de client car tout client enregistré
     * possède une entrée dans cette variable, si jamais il n'a jamais rien acheté alors l'entrée
     * pointe une liste vide car il n'a pas d'historique d'achat.
     */
    private HashMap<iArticle, Integer> stock;
    private HashMap<iClient, Commande> paniers;
    private HashMap<iClient, List<Commande>> commandes;

    /**
     * Le constructeur nous permet d'initialiser toutes nos variables.
     */
    public Magasin() {
        this.stock = new HashMap<>();
        this.paniers = new HashMap<>();
        this.commandes = new HashMap<>();
    }


    // iStock

    /**
     * Cette méthode nous permet de référencer un nouvel article au stock avec une quantité postive
     * ou nulle. Si la quantité est négative une QuantiteNegativeException sera levée. Tandis que
     * si l'article est déjà référencé au stock une ArticleDejaEnStockException sera levée.
     * @param nouvelArticle    l'article à référecner
     * @param quantiteNouvelle la quantite de l'article à ajouter. Cette quantite peut être nulle,
     *                         ce qui correspond à  uniquement referencer l'article
     * @throws ArticleDejaEnStockException
     * @throws QuantiteNegativeException
     */
    @Override
    public void referencerAuStock(iArticle nouvelArticle, int quantiteNouvelle)
            throws ArticleDejaEnStockException, QuantiteNegativeException {
        if (quantiteNouvelle < 0)
            throw new QuantiteNegativeException();
        if (this.stock.containsKey(nouvelArticle))
            throw new ArticleDejaEnStockException();
        this.stock.put(nouvelArticle, quantiteNouvelle);
    }

    /**
     * Cette méthode nous permet de réapprovisionner un article au stock avec une quantité postive.
     * Si la quantité est négative ou nulle une QuantiteNegativeOuNulleException sera levée.
     * Tandis que si l'article n'est pas déjà référencé au stock une ArticleHorsStockException
     * sera levée.
     * @param articleMaj      l'article à reapprovisionner
     * @param quantiteAjoutee la quantite de article à ajouter
     * @throws ArticleHorsStockException
     * @throws QuantiteNegativeOuNulleException
     */
    @Override
    public void reapprovisionnerStock(iArticle articleMaj, int quantiteAjoutee)
            throws ArticleHorsStockException, QuantiteNegativeOuNulleException {
        if (quantiteAjoutee <= 0)
            throw new QuantiteNegativeOuNulleException();
        if (!this.stock.containsKey(articleMaj))
            throw new ArticleHorsStockException();
        this.stock.replace(articleMaj, this.consulterQuantiteEnStock(articleMaj) + quantiteAjoutee);

    }

    /**
     * Cette méthode nous permet d'obtenir la quantité d'un article particulier.
     * Si l'article n'est pas déjà référencé au stock une ArticleHorsStockException sera levée.
     * @param articleRecherche l'article à considérer
     * @return quantity la quantité correspondant au stock de l'article considéré
     * @throws ArticleHorsStockException
     */
    @Override
    public int consulterQuantiteEnStock(iArticle articleRecherche) throws ArticleHorsStockException {
        if (!this.stock.containsKey(articleRecherche))
            throw new ArticleHorsStockException();
        return this.stock.get(articleRecherche);
    }

    /**
     * Cette méthode nous permet de retirer un article au stock avec une quantité postive.
     * Si la quantité est négative ou nulle une QuantiteNegativeOuNulleException sera levée.
     * Tandis que si l'article n'est pas déjà référencé au stock une ArticleHorsStockException
     * sera levée.
     * @param quantiteRetiree la quantité à retirer du stock
     * @param articleMaj      l'article à considérer
     * @throws ArticleHorsStockException
     * @throws QuantiteNegativeOuNulleException
     * @throws QuantiteEnStockInsuffisanteException
     */
    @Override
    public void retirerDuStock(int quantiteRetiree, iArticle articleMaj)
            throws ArticleHorsStockException, QuantiteNegativeOuNulleException, QuantiteEnStockInsuffisanteException {
        if (quantiteRetiree <= 0)
            throw new QuantiteNegativeOuNulleException();
        if (!this.stock.containsKey(articleMaj))
            throw new ArticleHorsStockException();
        int quantite = this.consulterQuantiteEnStock(articleMaj);
        if (quantite < quantiteRetiree)
            throw new QuantiteEnStockInsuffisanteException();
        this.stock.replace(articleMaj, quantite - quantiteRetiree);
    }

    /**
     * Cette methode permet d'obtenir les articles présent en magasin trié par nom.
     * @return articlesParNom la liste de tous les articles en stock trié par nom
     */
    @Override
    public List<iArticle> listerArticlesEnStockParNom() {
        List<iArticle> data = new ArrayList<>(this.stock.keySet());
        data.sort(iArticle.COMPARATEUR_NOM);
        return data;
    }

    /**
     * Cette methode permet d'obtenir les articles présent au magasin trié par référence.
     * @return articlesParReference la liste de tous les articles en stock trié par référence
     */
    @Override
    public List<iArticle> listerArticlesEnStockParReference() {
        List<iArticle> data = new ArrayList<>(this.stock.keySet());
        data.sort(iArticle.COMPARATEUR_REFERENCE);
        return data;
    }

    /**
     * Cette methode permet d'obtenir les stocks du magasin trié par nom d'articles.
     * @return stock la liste de tous les articles en stock et leur quantité trié par nom
     */
    @Override
    public List<Map.Entry<iArticle, Integer>> listerStock() {
        List<Map.Entry<iArticle, Integer>> data = new ArrayList<>(this.stock.entrySet());
        data.sort((o1, o2) -> iArticle.COMPARATEUR_NOM.compare(o1.getKey(), o2.getKey()));
        return data;
    }

    // iClientele

    /**
     * Cette methode permet d'enregistrer un client au sein du magasin.
     * Si celui-ci est déjà enregistré une ClientDejaEnregistreException sera levée.
     * @param nouveauClient le nouveau client à référencer
     * @throws ClientDejaEnregistreException
     */
    @Override
    public void enregistrerNouveauClient(iClient nouveauClient) throws ClientDejaEnregistreException {
        if (this.commandes.containsKey(nouveauClient))
            throw new ClientDejaEnregistreException();
        this.commandes.put(nouveauClient, new ArrayList<>());
    }

    /**
     * Cette methode retourne la liste  des clients du magasin trié par id
     * @return clientsParId la liste des clients tiré par id
     */
    @Override
    public List<iClient> listerLesClientsParId() {
        List<iClient> data = new ArrayList<>(this.commandes.keySet());
        data.sort(iClient.COMPARATEUR_ID);
        return data;
    }


    // iPanier

    /**
     * Cette méthode permet de consulter le panier d'un client donné.
     * Si celui-ci n'est pas déjà enregistré une ClientInconnuException sera levée.
     * @param client le client à considérer
     * @return panier la commande correspondant au client
     * @throws ClientInconnuException
     */
    @Override
    public Commande consulterPanier(iClient client) throws ClientInconnuException {
        if (!this.commandes.containsKey(client))
            throw new ClientInconnuException();
        return this.paniers.getOrDefault(client, new Commande());
    }

    /**
     * Cette méthode permet d'ajouter un produit donné, dans la quantité spécifiée au panier du client donné.
     * Si la quantité n'est pas positive une QuantiteEnStockInsuffisanteException sera levée.
     * Tandis que si l'article spécifié n'est pas en stock une ArticleHorsStockException sera levée.
     * Tandis que si l'article n'est pas en quantité suffisante pour permettre l'achat une
     * QuantiteEnStockInsuffisanteException sera levée.
     * Tandis que si le client n'est pas enregistré une ClientInconnuException sera levée.
     * @param client   le client à considérer
     * @param article  l'article à considérer
     * @param quantite la quantité à ajouter
     * @throws ClientInconnuException
     * @throws QuantiteNegativeOuNulleException
     * @throws ArticleHorsStockException
     * @throws QuantiteEnStockInsuffisanteException
     */
    @Override
    public void ajouterAuPanier(iClient client, iArticle article, int quantite)
            throws ClientInconnuException,
            QuantiteNegativeOuNulleException,
            ArticleHorsStockException, QuantiteEnStockInsuffisanteException {
        if (quantite <= 0)
            throw new QuantiteNegativeOuNulleException();
        if (!this.stock.containsKey(article))
            throw new ArticleHorsStockException();
        if (this.stock.get(article) < quantite)
            throw new QuantiteEnStockInsuffisanteException();
        if (!this.commandes.containsKey(client))
            throw new ClientInconnuException();
        this.paniers.putIfAbsent(client, new Commande());
        Commande cmd = this.paniers.get(client);
        cmd.ajout(quantite, article);
        this.paniers.replace(client, cmd);
        this.retirerDuStock(quantite, article);
    }

    /**
     * Cette méthode permet de retirer un produit donné du panier, dans la quantité spécifiée au panier
     * du client donné.
     * Si la quantité n'est pas positive une QuantiteEnStockInsuffisanteException sera levée.
     * Tandis que si l'article spécifié n'est pas en stock une ArticleHorsStockException sera levée.
     * Tandis que si le client n'est pas enregistré une ClientInconnuException sera levée.
     * Tandis que si l'article spécifié n'est pas au sein du panier une ArticleHorsPanierException
     * sera levée.
     * Tandis que si l'article spécifié n'est pas en quantitée suffisante dans le panier une
     * QuantiteSuppPanierException sera levée.
     * @param client   le client à considérer
     * @param quantite la quantité à supprimer
     * @param article  l'article à considérer
     * @throws ClientInconnuException
     * @throws QuantiteNegativeOuNulleException
     * @throws QuantiteSuppPanierException
     * @throws ArticleHorsPanierException
     * @throws ArticleHorsStockException
     */
    @Override
    public void supprimerDuPanier(iClient client, int quantite, iArticle article)
            throws ClientInconnuException,
            QuantiteNegativeOuNulleException,
            QuantiteSuppPanierException, ArticleHorsPanierException,
            ArticleHorsStockException {
        if (quantite <= 0)
            throw new QuantiteNegativeOuNulleException();
        if (!this.stock.containsKey(article))
            throw new ArticleHorsStockException();
        if (!this.commandes.containsKey(client))
            throw new ClientInconnuException();
        this.paniers.putIfAbsent(client, new Commande());
        Commande cmd = this.paniers.get(client);
        cmd.retirer(quantite, article);
        this.paniers.replace(client, cmd);
        this.reapprovisionnerStock(article, quantite);
    }

    /**
     * Méthode retournant le montant du panier du client spécifié.
     * Si le client n'est pas enregistré une ClientInconnuException sera levée.
     * @param client le client à considérer
     * @return montant le montant du panier
     * @throws ClientInconnuException
     */
    @Override
    public double consulterMontantPanier(iClient client) throws ClientInconnuException {
        if (!this.commandes.containsKey(client))
            throw new ClientInconnuException();
        this.paniers.putIfAbsent(client, new Commande());
        return this.paniers.get(client).montant();
    }

    /**
     * Méthode permettant de vider le panier du client spécifié.
     * Si le client n'est pas enregistré une ClientInconnuException sera levée.
     * @param client le client à considérer
     * @throws ClientInconnuException
     */
    @Override
    public void viderPanier(iClient client) throws ClientInconnuException {
        if (!this.commandes.containsKey(client))
            throw new ClientInconnuException();
        this.paniers.putIfAbsent(client, new Commande());
        for (Map.Entry<iArticle, Integer> item : this.paniers.get(client).listerCommande()) {
            try {
                this.supprimerDuPanier(client, item.getValue(), item.getKey());
            }
            catch (ArticleHorsStockException | QuantiteNegativeOuNulleException | QuantiteSuppPanierException | ArticleHorsPanierException ignored) {
                ;
            }
        }
    }

    /**
     * Méthode permettant de finaliser le panier du client spécifié. Ainsi sa commande
     * passe alors dans son historique d'achat et un nouveau panier lui est attribué.
     * Si le client n'est pas enregistré une ClientInconnuException sera levée.
     * @param client le client à considérer
     * @throws ClientInconnuException
     */
    @Override
    public void terminerLaCommande(iClient client) throws ClientInconnuException {
        if (!this.commandes.containsKey(client))
            throw new ClientInconnuException();
        this.paniers.putIfAbsent(client, new Commande());
        Commande cmd = this.paniers.get(client);
        if (!cmd.estVide()) {
            List<Commande> cmds = this.commandes.get(client);
            cmds.add(cmd);
            this.commandes.replace(client, cmds);
            this.paniers.replace(client, new Commande());
        }
    }

    /**
     * Méthode retournant l'historique de commande du client spécifié.
     * Si le client n'est pas enregistré une ClientInconnuException sera levée.
     * @param client le client à considérer
     * @return commandes la liste des commandes terminées pour le client spécifié
     * @throws ClientInconnuException
     */
    @Override
    public List<Commande> listerCommandesTerminees(iClient client) throws ClientInconnuException {
        if (!this.commandes.containsKey(client))
            throw new ClientInconnuException();
        List<Commande> cmds = this.commandes.get(client);
        cmds.sort(Commande::compareTo);
        return cmds;
    }

    /**
     * Méthode retournant le montant lié à l'historique de commande du client spécifié.
     * Si le client n'est pas enregistré une ClientInconnuException sera levée.
     * @param client le client à considérer
     * @return montant le montant du panier
     * @throws ClientInconnuException
     */
    @Override
    public double consulterMontantTotalCommandes(iClient client) throws ClientInconnuException {
        if (!this.commandes.containsKey(client))
            throw new ClientInconnuException();
        double price = 0;
        for (Commande cmd: this.commandes.get(client))
            price += cmd.montant();
        return price;
    }


}