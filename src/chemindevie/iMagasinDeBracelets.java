package chemindevie;

import magasin.exceptions.*;
import magasin.iArticle;
import magasin.iClient;

import java.util.List;
import java.util.Map;

public interface iMagasinDeBracelets {


    /**
     * permet d'alimenter le stock de pierres du magasin
     *
     * @param pierre   la sorte de pierre
     * @param quantite la quantité de pierres
     * @throws QuantiteNegativeOuNulleException si la quantité indiquée est incorrecte
     */
    void ajouterDesPierres(Pierre pierre, int quantite)
            throws QuantiteNegativeOuNulleException;

    List<Map.Entry<iArticle, Integer>> listerLesPierresEnStock();

    /**
     * enregistrer un nouveau client
     *
     * @param client le client
     * @throws ClientDejaEnregistreException si le client est déjà enregistré
     */
    void enregistrerUnClient(iClient client) throws ClientDejaEnregistreException;

    /**
     * importer des clients depuis le webservice RandomUser
     *
     * @param nbClients le nombre de clients à récupérer
     */
    void importerDesClientsDepuisRandomUser(int nbClients);


    /**
     * lister les clients enregistrés
     *
     * @return la liste des clients enregistrés
     */
    List<iClient> listerLesClients();


    /**
     * @param bracelet le bracelet à ajouter au panier
     * @param client   le client
     * @throws ClientInconnuException               si le client n'est pas encore enregistré
     * @throws QuantiteEnStockInsuffisanteException s'il n'y a pas assez de pierres en stock
     * @throws ArticleHorsStockException            si (au moins) une des pierres du bracelet n'est pas en stock
     */
    void ajouterUnBracelet(Bracelet bracelet, iClient client) throws ClientInconnuException,
            QuantiteEnStockInsuffisanteException, ArticleHorsStockException;

    /**
     * donne le montant du panier actuel du client
     *
     * @param client le client
     * @return
     * @throws ClientInconnuException si le client n'est pas encore enregistré
     */
    double montantDuPanier(iClient client) throws ClientInconnuException;
}
