package chemindevie;

import magasin.Magasin;
import magasin.exceptions.*;
import magasin.iArticle;
import magasin.iClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MagasinDeBracelets implements iMagasinDeBracelets {

    Magasin magasin;

    /**
     * Constructeur pour initialiser notre magasin
     */
    MagasinDeBracelets() {
        this.magasin = new Magasin();
    }

    /**
     * Méthode pour ajouter des pierres aux stocks.
     * Si la quantité n'est pas positive une QuantiteNegativeOuNulleException est levée.
     * @param pierre   la sorte de pierre
     * @param quantite la quantité de pierres
     * @throws QuantiteNegativeOuNulleException
     */
    @Override
    public void ajouterDesPierres(Pierre pierre, int quantite)
            throws QuantiteNegativeOuNulleException {
        try {
            this.magasin.referencerAuStock(pierre, quantite);
        } catch (ArticleDejaEnStockException except) {
            try {
                this.magasin.reapprovisionnerStock(pierre, quantite);
            } catch (ArticleHorsStockException ignored) {

            }
        }
    }

    /**
     * Méthode permettant de consulter l'état de notre stock de pierre
     * @return stockDePierre liste de pierre et que leur quantité trié par reference
     */
    @Override
    public List<Map.Entry<iArticle, Integer>> listerLesPierresEnStock() {
        List<Map.Entry<iArticle, Integer>> out = new ArrayList<>();
        for (Map.Entry<iArticle, Integer> entry: this.magasin.listerStock())
            if (Pierre.class == entry.getKey().getClass())
                out.add(entry);
        out.sort((o1, o2) -> iArticle.COMPARATEUR_REFERENCE.compare(o1.getKey(), o2.getKey()));
        return out;
    }

    /**
     * Méthode pour enregistrer un client au sein du magasin.
     * Si le client est déjà enregistré une ClientDejaEnregistreException est levée.
     * @param client le client
     * @throws ClientDejaEnregistreException
     */
    @Override
    public void enregistrerUnClient(iClient client) throws ClientDejaEnregistreException {
        this.magasin.enregistrerNouveauClient(client);
    }

    /**
     * Méthode pour consulter la liste de client du magasin.
     * @return clientsParId liste des clients triés par id
     */
    @Override
    public List<iClient> listerLesClients() {
        return this.magasin.listerLesClientsParId();
    }

    /**
     * Méthode pour peupler notre magasin de clients
     * @param nbClients le nombre de clients à récupérer
     */
    @Override
    public void importerDesClientsDepuisRandomUser(int nbClients) {
        int failure = 0;
        for (Client client: Client.createClients(nbClients))
            try {
                this.enregistrerUnClient(client);
            } catch (ClientDejaEnregistreException exc) {
                failure += 1;
            }
        if (failure > 0)
            importerDesClientsDepuisRandomUser(failure);
    }

    /**
     * Méthode pour ajouter un bracelet au panier d'un client spécifié.
     * Si l'une des pierre n'est pas présente en stock une ArticleHorsStockException est levée.
     * Tandis que si l'une des pierres est en quantité insuffisante une
     * QuantiteEnStockInsuffisanteException est levée.
     * Tandis que si le client n'est pas enregistré une ClientInconnuException est levée.
     * @param bracelet le bracelet à ajouter au panier
     * @param client   le client
     * @throws ClientInconnuException
     * @throws QuantiteEnStockInsuffisanteException
     * @throws ArticleHorsStockException
     */
    @Override
    public void ajouterUnBracelet(Bracelet bracelet, iClient client) throws ClientInconnuException,
            QuantiteEnStockInsuffisanteException, ArticleHorsStockException {
        for (Pierre pierre: bracelet.pierres()) {
            try {
                this.magasin.ajouterAuPanier(client, pierre, 1);
            } catch (QuantiteNegativeOuNulleException ignored) {
                ;
            }
        }
    }

    /**
     * Méthode pour visualiser le montant du panier du client spécifié.
     * Si le client n'est pas enregistré une ClientInconnuException est levée.
     * @param client le client
     * @return montant tarif du panier
     * @throws ClientInconnuException
     */
    @Override
    public double montantDuPanier(iClient client) throws ClientInconnuException {
        return this.magasin.consulterMontantPanier(client);
    }


}
