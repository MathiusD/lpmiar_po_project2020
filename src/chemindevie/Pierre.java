package chemindevie;

import magasin.iArticle;

public enum Pierre implements iArticle {

    QUARTZ_ROSE(1, "Quartz rose"),
    JASPE(2, "Jaspe rouge"),
    CALCEDOINE(3, "Calcédoine"),
    JADE(4, "Jade"),
    EMERAUDE(5, "Émeraude"),
    GRENAT(6, "Grenat"),
    CITRINE(7, "Citrine"),
    OBSIDIENNE(8, "Obsidienne"),
    AIGUE_MARINE(9, "Aigue-marine"),
    RHODOCHROSITE(10, "Rhodochrosite"),
    CORNALINE(11, "Cornaline"),
    AMBRE(12, "Ambre"),
    HEMATITE(13, "Hématite"),
    AMETHYSTE(14, "Améthyste"),
    MALACHITE(15, "Malachite"),
    OPALE(16, "Opale"),
    TURQUOISE(17, "Turquoise"),
    PIERRE_LUNE(18, "Pierre de lune"),
    TOPAZE(19, "Topaze"),
    LAPIS_LAZULI(20, "Lapis lazuli"),
    TOURMALINE(21, "Tourmaline"),
    CRISTAL(22, "Cristal de roche"),
    AZURITE(23, "Azurite"),
    AMAZONITE(24, "Amazonite"),
    OEIL_TIGRE(25, "Œil de tigre"),
    PYRITE(26, "Pyrite"),
    FLUORITE(27, "Fluorine ou fluorite"),
    PERLE(28, "Perle"),
    SODALITE(29, "Sodalite"),
    QUARTZ_FUME(30, "Quartz fumé"),
    SOUFRE(31, "Soufre"),
    MERCURE(32, "Mercure"),
    SEL(33, "Sel");

    private int numero;
    private String description;
    private double prix;

    Pierre(int num, String desc, double prix) {
        this.numero = num;
        this.description = desc;
        this.prix = prix;
    }

    Pierre(int num, String nom) {
        this(num, nom, 15.99);
    }

    @Override
    public int reference() {
        return numero;
    }

    @Override
    public String nom() {
        return description;
    }

    @Override
    public double prix() {
        return prix;
    }
}
