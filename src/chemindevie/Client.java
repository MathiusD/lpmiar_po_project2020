package chemindevie;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.json.JSONArray;
import kong.unirest.json.JSONObject;
import magasin.iClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Client implements iClient {

    private String nom;
    private String prenom;
    private String email;
    private String username;

    public Client(String nom, String prenom, String email, String username) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.username = username;
    }

    @Override
    public String id() {
        return username;
    }

    @Override
    public int compareTo(iClient o) {
        return iClient.COMPARATEUR_ID.compare(this, o);
    }

    /**
     * Méthode permettant de générer n clients.
     * @param nbUsers
     * @return users liste des clients générés
     */
    public static List<Client> createClients(int nbUsers) {
        int exec = 0;
        List<Client> clients = new ArrayList<>();
        HttpResponse<String> request = Unirest.get(String.format("https://randomuser.me/api/?nat=fr&results=%s", nbUsers)).asString();
        if (request.getStatus() == 200) {
            JSONObject raw = new JsonNode(request.getBody()).getObject();
            Set<String> rawKeys = raw.keySet();
            if (rawKeys.contains("results")) {
                JSONArray rawData = raw.getJSONArray("results");
                if (!rawData.isEmpty()) {
                    for (int index = 0; index < rawData.length(); index++) {
                        JSONObject json = rawData.getJSONObject(index);
                        Set<String> keys = json.keySet();
                        if (keys.contains("name") && keys.contains("email") && keys.contains("login")) {
                            JSONObject name = json.getJSONObject("name");
                            JSONObject login = json.getJSONObject("login");
                            Set<String> nameKeys = name.keySet();
                            Set<String> loginKeys = login.keySet();
                            if (nameKeys.contains("first") && nameKeys.contains("last") && loginKeys.contains("username")) {
                                clients.add(new Client(name.getString("last"), name.getString("first"), json.getString("email"), login.getString("username")));
                                exec += 1;
                            }
                        }
                    }
                }
            }
        }
        if (exec != nbUsers)
            clients.addAll(createClients(nbUsers - exec));
        return clients;
    }
}
