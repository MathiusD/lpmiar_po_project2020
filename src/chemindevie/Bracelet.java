package chemindevie;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Bracelet {

    private List<Pierre> pierres;

    /**
     * Constructeur pour instancier un bracelet avec les 8 pierres souhaitée
     * @param base
     * @param sommet
     * @param cheminVie
     * @param appel
     * @param personnalite
     * @param expression
     * @param voeux
     * @param touche
     */
    private Bracelet(Pierre base, Pierre sommet,
                     Pierre cheminVie, Pierre appel,
                     Pierre personnalite, Pierre expression,
                     Pierre voeux, Pierre touche) {
        this.pierres = new ArrayList<>();
        this.pierres.add(base);
        this.pierres.add(sommet);
        this.pierres.add(cheminVie);
        this.pierres.add(appel);
        this.pierres.add(personnalite);
        this.pierres.add(expression);
        this.pierres.add(voeux);
        this.pierres.add(touche);

    }


    /**
     * Fabrique un bracelet de vie correspondant à la personne décrite par les différentes informations indiquées.
     *
     * @param prenoms     la liste des prénoms
     * @param nomDuPere   le nom du père
     * @param nomDeLaMere le nom de la mère
     * @param jour        le jour de naissance
     * @param mois        le mois de naissance
     * @param annee       l'année de naissance
     * @return
     */
    public static Bracelet calculer(List<String> prenoms,
                                    String nomDuPere,
                                    String nomDeLaMere,
                                    int jour, int mois, int annee) {
        return new Bracelet(
            Bracelet.PierreDeBase(prenoms, nomDuPere, nomDeLaMere),
            Bracelet.PierreDeSommet(prenoms, nomDuPere, nomDeLaMere),
            Bracelet.PierreDeCheminDeVie(jour, mois, annee),
            Bracelet.PierreDAppel(prenoms, nomDuPere, nomDeLaMere),
            Bracelet.PierreDePersonnalite(prenoms, nomDuPere, nomDeLaMere),
            Bracelet.PierreDExpression(prenoms, nomDuPere, nomDeLaMere),
            Bracelet.PierreDeVoeux(prenoms, nomDuPere, nomDeLaMere),
            Bracelet.PierreDeTouche(prenoms, nomDuPere, nomDeLaMere, jour, mois, annee)
        );
    }


    /**
     * donne la liste des pierres du bracelet de vie, dans l'ordre :
     * 1. pierre de base, 2. pierre de sommet, 3. pierre de chemin vie,
     * 4. pierre d'appel, 5. pierre de personnalité, 6. pierre d'expression,
     * 7. pierre de voeux et 8. pierre de touche
     *
     * @return la liste des pierres du bracelet de vie
     */
    public List<Pierre> pierres() {
        return this.pierres;
    }

    /**
     * Méthode permettant d'extraire le premier caractère d'une chaine de caractères
     * @param data
     * @return char
     */
    private static String extractFirst(String data) {
        return String.format("%c", data.charAt(0));
    }

    /**
     * Méthode permettant d'extraire le dernier caractère d'une chaine de caractère
     * @param data
     * @return char
     */
    private static String extractLast(String data) {
        return String.format("%c", data.charAt(data.length() - 1));
    }

    /**
     * Méthode permettant d'obtenir tout les coefs liés au calcul de la Pierre de Base.
     * @param prenoms
     * @param nomDuPere
     * @param nomDeLaMere
     * @return coefs
     */
    private static List<Integer> coefsBase(List<String> prenoms, String nomDuPere, String nomDeLaMere) {
        List<String> chars = new ArrayList<>();
        for (String prenom: prenoms)
            chars.add(Bracelet.extractFirst(prenom));
        chars.add(Bracelet.extractFirst(nomDuPere));
        chars.add(Bracelet.extractFirst(nomDeLaMere));
        return Bracelet.coefsLettre(chars);
    }

    /**
     * Méthode permettant d'obtenir la valeur de la Pierre de Base
     * @param prenoms
     * @param nomDuPere
     * @param nomDeLaMere
     * @return coefBase
     */
    private static int coefBase(List<String> prenoms, String nomDuPere, String nomDeLaMere) {
        return Bracelet.sumCoef(Bracelet.coefsBase(prenoms, nomDuPere, nomDeLaMere));
    }

    /**
     * Méthode permettant d'obtenir la Pierre de Base
     * @param prenoms
     * @param nomDuPere
     * @param nomDeLaMere
     * @return pierreDeBase
     */
    private static Pierre PierreDeBase(List<String> prenoms, String nomDuPere, String nomDeLaMere) {
        return Pierre.values()[Bracelet.coefBase(prenoms, nomDuPere, nomDeLaMere) - 1];
    }

    /**
     * Méthode permettant d'obtenir tout les coefs liés au calcul de la Pierre de Sommet.
     * @param prenoms
     * @param nomDuPere
     * @param nomDeLaMere
     * @return coefs
     */
    private static List<Integer>  coefsSommet(List<String> prenoms, String nomDuPere, String nomDeLaMere) {
        List<String> chars = new ArrayList<>();
        for (String prenom: prenoms)
            chars.add(Bracelet.extractLast(prenom));
        chars.add(Bracelet.extractLast(nomDuPere));
        chars.add(Bracelet.extractLast(nomDeLaMere));
        return Bracelet.coefsLettre(chars);
    }

    /**
     * Méthode permettant d'obtenir la valeur de la Pierre de Sommet
     * @param prenoms
     * @param nomDuPere
     * @param nomDeLaMere
     * @return coefSommet
     */
    private static int coefSommet(List<String> prenoms, String nomDuPere, String nomDeLaMere) {
        return Bracelet.sumCoef(Bracelet.coefsSommet(prenoms, nomDuPere, nomDeLaMere));
    }

    /**
     * Méthode permettant d'obtenir la Pierre de Sommet
     * @param prenoms
     * @param nomDuPere
     * @param nomDeLaMere
     * @return pierreDeSommet
     */
    private static Pierre PierreDeSommet(List<String> prenoms, String nomDuPere, String nomDeLaMere) {
        return Pierre.values()[Bracelet.coefSommet(prenoms, nomDuPere, nomDeLaMere) - 1];
    }

    /**
     * Méthode permettant d'obtenir tout les coefs liés au calcul de la Pierre de Vie.
     * @param jour
     * @param mois
     * @param annee
     * @return
     */
    private static List<Integer> coefsVie(int jour, int mois, int annee) {
        int sum = jour + mois + annee;
        return Bracelet.digits(sum);
    }

    /**
     * Méthode permettant d'obtenir la valeur de la Pierre de Vie
     * @param jour
     * @param mois
     * @param annee
     * @return
     */
    private static int coefVie(int jour, int mois, int annee) {
        return Bracelet.sumCoef(Bracelet.coefsVie(jour, mois, annee));
    }

    /**
     * Méthode permettant d'obtenir la valeur de la Pierre de Chemin de Vie
     * @param jour
     * @param mois
     * @param annee
     * @return
     */
    private static Pierre PierreDeCheminDeVie(int jour, int mois, int annee) {
        return Pierre.values()[Bracelet.coefVie(jour, mois, annee) - 1];
    }

    /**
     * Méthode permettant d'obtenir tout les coefs liés au calcul de la Pierre d'Appel.
     * @param prenoms
     * @param nomDuPere
     * @param nomDeLaMere
     * @return
     */
    private static List<Integer> coefsAppel(List<String> prenoms, String nomDuPere, String nomDeLaMere) {
        List<String> datas = new ArrayList<>(prenoms);
        datas.add(nomDuPere);
        datas.add(nomDeLaMere);
        List<String> chars = new ArrayList<>();
        for (String data: datas)
            for (char car : data.toUpperCase().toCharArray())
                if (Bracelet.isVoyelle(car))
                    chars.add(String.format("%c", car));
        return Bracelet.coefsLettre(chars);
    }

    /**
     * Méthode permettant d'obtenir la valeur de la Pierre d'Appel
     * @param prenoms
     * @param nomDuPere
     * @param nomDeLaMere
     * @return
     */
    private static int coefAppel(List<String> prenoms, String nomDuPere, String nomDeLaMere) {
        return Bracelet.sumCoef(Bracelet.coefsAppel(prenoms, nomDuPere, nomDeLaMere));
    }

    /**
     * Méthode permettant d'obtenir la valeur de la Pierre d'Appel
     * @param prenoms
     * @param nomDuPere
     * @param nomDeLaMere
     * @return
     */
    private static Pierre PierreDAppel(List<String> prenoms, String nomDuPere, String nomDeLaMere) {
        return Pierre.values()[Bracelet.coefAppel(prenoms, nomDuPere, nomDeLaMere) - 1];
    }

    /**
     * Méthode permettant d'obtenir tout les coefs liés au calcul de la Pierre de Personnalité.
     * @param prenoms
     * @param nomDuPere
     * @param nomDeLaMere
     * @return
     */
    private static List<Integer> coefsPersona(List<String> prenoms, String nomDuPere, String nomDeLaMere) {
        List<String> datas = new ArrayList<>(prenoms);
        datas.add(nomDuPere);
        datas.add(nomDeLaMere);
        List<String> chars = new ArrayList<>();
        for (String data: datas)
            for (char car : data.toUpperCase().toCharArray())
                if (!Bracelet.isVoyelle(car))
                    chars.add(String.format("%c", car));
        return Bracelet.coefsLettre(chars);
    }

    /**
     * Méthode permettant d'obtenir la valeur de la Pierre de Personnalité
     * @param prenoms
     * @param nomDuPere
     * @param nomDeLaMere
     * @return
     */
    private static int coefPersona(List<String> prenoms, String nomDuPere, String nomDeLaMere) {
        return Bracelet.sumCoef(Bracelet.coefsPersona(prenoms, nomDuPere, nomDeLaMere));
    }

    /**
     * Méthode permettant d'obtenir la valeur de la Pierre de Personnalité
     * @param prenoms
     * @param nomDuPere
     * @param nomDeLaMere
     * @return
     */
    private static Pierre PierreDePersonnalite(List<String> prenoms, String nomDuPere, String nomDeLaMere) {
        return Pierre.values()[Bracelet.coefPersona(prenoms, nomDuPere, nomDeLaMere) - 1];
    }

    /**
     * Méthode permettant d'obtenir tout les coefs liés au calcul de la Pierre d'Expression.
     * @param prenoms
     * @param nomDuPere
     * @param nomDeLaMere
     * @return
     */
    public static List<Integer> coefsExpression(List<String> prenoms, String nomDuPere, String nomDeLaMere) {
        List<Integer> coefs = new ArrayList<>();
        coefs.add(Bracelet.coefAppel(prenoms, nomDuPere, nomDeLaMere));
        coefs.add(Bracelet.coefPersona(prenoms, nomDuPere, nomDeLaMere));
        return coefs;
    }

    /**
     * Méthode permettant d'obtenir la valeur de la Pierre d'Expression
     * @param prenoms
     * @param nomDuPere
     * @param nomDeLaMere
     * @return
     */
    private static int coefExpression(List<String> prenoms, String nomDuPere, String nomDeLaMere) {
        return Bracelet.sumCoef(Bracelet.coefsExpression(prenoms, nomDuPere, nomDeLaMere));
    }

    /**
     * Méthode permettant d'obtenir la valeur de la Pierre d'Expression
     * @param prenoms
     * @param nomDuPere
     * @param nomDeLaMere
     * @return
     */
    private static Pierre PierreDExpression(List<String> prenoms, String nomDuPere, String nomDeLaMere) {
        return Pierre.values()[Bracelet.coefExpression(prenoms, nomDuPere, nomDeLaMere) - 1];
    }

    /**
     * Méthode permettant d'obtenir tout les coefs liés au calcul de la Pierre de Voeux.
     * @param prenoms
     * @param nomDuPere
     * @param nomDeLaMere
     * @return
     */
    public static List<Integer> coefsVoeux(List<String> prenoms, String nomDuPere, String nomDeLaMere) {
        List<String> datas = new ArrayList<>(prenoms);
        datas.add(nomDuPere);
        datas.add(nomDeLaMere);
        List<String> chars = new ArrayList<>();
        for (String data: datas)
            for (int compt = 0; compt < data.toUpperCase().toCharArray().length && chars.size() == datas.indexOf(data); compt++)
                if (Bracelet.isVoyelle(data.toUpperCase().toCharArray()[compt]))
                    chars.add(String.format("%c", data.toUpperCase().toCharArray()[compt]));
        return Bracelet.coefsLettre(chars);
    }

    /**
     * Méthode permettant d'obtenir la valeur de la Pierre d'Expression
     * @param prenoms
     * @param nomDuPere
     * @param nomDeLaMere
     * @return
     */
    private static int coefVoeux(List<String> prenoms, String nomDuPere, String nomDeLaMere) {
        return Bracelet.sumCoef(Bracelet.coefsVoeux(prenoms, nomDuPere, nomDeLaMere));
    }

    /**
     * Méthode permettant d'obtenir la valeur de la Pierre de Voeux
     * @param prenoms
     * @param nomDuPere
     * @param nomDeLaMere
     * @return
     */
    private static Pierre PierreDeVoeux(List<String> prenoms, String nomDuPere, String nomDeLaMere) {
        return Pierre.values()[Bracelet.coefVoeux(prenoms, nomDuPere, nomDeLaMere) - 1];
    }

    /**
     * Méthode permettant d'obtenir tout les coefs liés au calcul de la Pierre de Touche.
     * @param prenoms
     * @param nomDuPere
     * @param nomDeLaMere
     * @param jour
     * @param mois
     * @param annee
     * @return
     */
    public static List<Integer> coefsTouche(List<String> prenoms, String nomDuPere, String nomDeLaMere, int jour, int mois, int annee) {
        List<Integer> coefs = new ArrayList<>();
        coefs.add(Bracelet.coefBase(prenoms, nomDuPere, nomDeLaMere));
        coefs.add(Bracelet.coefSommet(prenoms, nomDuPere, nomDeLaMere));
        coefs.add(Bracelet.coefVie(jour, mois, annee));
        coefs.add(Bracelet.coefAppel(prenoms, nomDuPere, nomDeLaMere));
        coefs.add(Bracelet.coefPersona(prenoms, nomDuPere, nomDeLaMere));
        coefs.add(Bracelet.coefExpression(prenoms, nomDuPere, nomDeLaMere));
        coefs.add(Bracelet.coefVoeux(prenoms,  nomDuPere, nomDeLaMere));
        return coefs;
    }

    /**
     * Méthode permettant d'obtenir la valeur de la Pierre de Touche
     * @param prenoms
     * @param nomDuPere
     * @param nomDeLaMere
     * @param jour
     * @param mois
     * @param annee
     * @return
     */
    private static int coefTouche(List<String> prenoms, String nomDuPere, String nomDeLaMere, int jour, int mois, int annee) {
        return Bracelet.sumCoef(Bracelet.coefsTouche(prenoms, nomDuPere, nomDeLaMere, jour, mois, annee));
    }

    /**
     * Méthode permettant d'obtenir la valeur de la Pierre de Touche
     * @param prenoms
     * @param nomDuPere
     * @param nomDeLaMere
     * @param jour
     * @param mois
     * @param annee
     * @return
     */
    private static Pierre PierreDeTouche(List<String> prenoms, String nomDuPere, String nomDeLaMere, int jour, int mois, int annee) {
        return Pierre.values()[Bracelet.coefTouche(prenoms, nomDuPere, nomDeLaMere, jour, mois, annee) - 1];
    }

    /**
     * Méthode permettant de savoir si un caractère est une voyelle
     * @param c
     * @return isVoyelle
     */
    public static boolean isVoyelle(char c) {
        return "AEIOUY".indexOf(c) != -1;
    }

    /**
     * Méthode permettant d'obtenir la valeur d'un pierre à partir des lettres nécessaire au calcul
     * @param lettres
     * @return value
     */
    private static int sumLettres(List<String> lettres) {
        return Bracelet.sumCoef(Bracelet.coefsLettre(lettres));
    }

    /**
     * Méthode permettant d'obtenirs la liste de coefs correspondant à la liste de lettres
     * @param lettres
     * @return coefs
     */
    private static List<Integer> coefsLettre(List<String> lettres) {
        List<Integer> coefs = new ArrayList<>();
        for (String lettre : lettres)
            coefs.add(Bracelet.coefLettre(lettre));
        return coefs;
    }

    /**
     * Méthode permettant de découper un nombre afin d'en extraire ses chiffres
     * @param data
     * @return coefs
     */
    private static List<Integer> digits(int data) {
        List<Integer> coefs = new ArrayList<>();
        while (data != 0) {
            coefs.add(data % 10);
            data = data / 10;
        }
        Collections.reverse(coefs);
        return coefs;
    }

    /**
     * Méthode permettant d'obtenir la valeur d'un pierre à partir de ses coefs
     * @param coefs
     * @return value
     */
    private static int sumCoef(List<Integer> coefs) {
        int sum = 0;
        for (int compt = 0; compt < coefs.size(); compt ++)
            sum += coefs.get(compt);
        if (sum > 33)
            return Bracelet.sumCoef(Bracelet.digits(sum));
        return sum;
    }

    /**
     * Méthode permettant d'obtenir la valeur d'une lettre
     * @param lettre
     * @return valueLettre
     */
    private static int coefLettre(String lettre) {
        char data = lettre.toUpperCase().charAt(0);
        if (data == 'A' || data == 'J' || data == 'S')
            return 1;
        if (data == 'B' || data == 'K' || data == 'T')
            return 2;
        if (data == 'C' || data == 'L' || data == 'U')
            return 3;
        if (data == 'D' || data == 'M' || data == 'V')
            return 4;
        if (data == 'E' || data == 'N' || data == 'W')
            return 5;
        if (data == 'F' || data == 'O' || data == 'X')
            return 6;
        if (data == 'G' || data == 'P' || data == 'Y')
            return 7;
        if (data == 'H' || data == 'Q' || data == 'Z')
            return 8;
        if (data == 'I' || data == 'R')
            return 9;
        return 0;
    }
}
