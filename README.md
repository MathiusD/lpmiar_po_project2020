# Mini-projet : magasin de bracelets "chemin de vie"

Ce mini-projet est constitué de plusieurs parties. Les parties 1
et 2 sont indépendantes. La partie 3 réutilise les parties 1 et 2.

Vous travaillerez en binôme en évitant de trop communiquer 
de vive voix (Covid oblige).

**Durée :** la matinée.

**Rendu :**  vous renommerez le dossier `src/` 
en `src_<NOM1>_<NOM2>/` (et uniquement le dossier `src/`)
 puis vous compresserez le dossier renommé (format `.zip`) ; 
 finalement envoi par email à 
 [lanoix-a@univ-nantes.fr](mailto:lanoix-a@univ-nantes.fr)
 



### Partie 1 : magasin générique (structure de données)

Implémentez les fonctionnalités attendues par un outil 
de gestion d'un magasin : 
stock de produits, 
panier et historique des commandes client, etc.

La classe `Magasin` qui proposera des `iArticle`s 
défini abstraitement à des `iClient`s. 
Précisément, dans la classe `Magasin` : 

1. Implémentez les méthodes de l'interface `iSock` ;

2. Implémentez les méthodes de l'interface `iClientele` ;

3. Implémentez les méthodes de l'interface `iPanier` ;
Nécessite égalament d'implémenter la classe `Commande`.

Quand on instanciera la classe `Magasin` il faudra 
lui fournir de vrais articles pour de vrais clients.

---

### Partie 2 : bracelets "chemin de vie" (algorithmique)

En lithothérapie, un bracelet "chemin de vie" est 
un bracelet constitué de 8 pierres parmi 33 pierres possibles,
chaque pierre ayant des "vertus énergétiques". 

Un bracelet "chemin de vie" est "unique"
chaque pierre étant défini par des "calculs basés pour une personne donné 
sur 
+ son (ses) prénom(s), 
+ le nom de famille de son père,
+ le nom de jeune fille de sa mère, et 
+ sa date de naissance (jour, mois et année)

Complétez la classe `Bracelet` qui représente un bracelet de vie.

Les détails des calculs sont donnés dans [BraceletPierreDeVie.md](BraceletPierreDeVie.md)

---

### Partie 3 : magasin de bracelets "chemin de vie"

Implémentez dans la classe `MagasinDeBracelets` 
les fonctionnalités de base d'un magasin qui vend
des bracelets de vie, en utilisant la classe `Magasin` 
développée précédemment.



