Données :
+ son (ses) prénom(s), 
+ le nom de famille de son père,
+ le nom de jeune fille de sa mère, et 
+ sa date de naissance (jour, mois et année)

On va calculer une série de nombres à partir des données précédentes: 
chaque nombre calculé correspondra à une pierre.


**Prémisse :** chaque lettre est associée à un chiffre :

|1	|2	|3	|4	|5	|6	|7	|8	|9  |
|---|---|---|---|---|---|---|---|---|
|A	|B	|C	|D	|E	|F	|G	|H	|I  |
|J	|K	|L	|M	|N	|O	|P	|Q	|R  |
|S	|T	|U	|V	|W	|X	|Y	|Z	|   |

**Pierre de base :**
1. somme des premières lettres de tous les noms (prénoms+nom de Famille + nom de jeune fille).
2. si le résultat dépasse 33, il faut réduire, c'est-à-dire faire la somme
des chiffres du résultat, jusqu'à obtenir un nombre <= 33

Exemple : Si 37 alors 3 + 7 = 10

**Pierre de sommet :**
1. somme des dernières lettres de tous les noms 
2. réduction si nécessaire

**Pierre de chemin de vie :** 

1. somme des nombres de votre date de naissance
2. réduction 

Exemple : 21 + 08 + 1978 = 2007 ; puis  2 + 0 + 0 + 7 = 9.

**Pierre d’appel :** 

1. somme des chiffres liés uniquement aux voyelles de tous les noms
2. réduction 

**Pierre de personnalité :**

1. somme des chiffres liés uniquement aux consonnes de tous les noms 
2. réduction

**Pierre d’expression :** 

1. somme des résultats trouvés pour la pierre d’appel et la pierre de personnalité
2. réduction

**Pierre de voeux :** 

1. somme des chiffres liés uniquement aux premières voyelles de vos noms et prénoms
2. réduction 

**pierre de touche :** 

1. somme des résultats trouvés pour toutes les autres pierres 
2. réduction

Finalement, la correspondance Nombre->Pierre est obtenue ainsi :
1. Quartz rose
2. Jaspe rouge
3. Calcédoine
4. Jade
5. Émeraude
6. Grenat
7. Citrine
8. Obsidienne
9. Aigue-marine
10. Rhodochrosite
11. Cornaline
12. Ambre
13. Hématite
14. Améthyste
15. Malachite
16. Opale
17. Turquoise
18. Pierre de lune
19. Topaze
20. Lapis lazuli
21. Tourmaline
22. Cristal de roche
23. Azurite
24. Amazonite
25. Œil de tigre
26. Pyrite
27. Fluorine ou fluorite
28. Perle
29. Sodalite
30. Quartz fumé
31. Soufre
32. Mercure
33. Sel

